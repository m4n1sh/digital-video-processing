function psnr = computer_psnr(targetImage, predictedImage)
    [height, width, depth] = size(targetImage);
    
    % We need to sum up the error. Initialize it to 0
    total_error = 0;
    
    % Now we need to iterate over all the pixels in both the images
    for x = 1:height
        for y = 1:width
            inc_error = 0.0;
            if depth == 1
                inc_error = (double(targetImage(x,y)) - double(predictedImage(x,y)))^2;
            end
            if depth == 3
                inc_error_r = (double(targetImage(x,y,1)) - double(predictedImage(x,y,1)))^2;
                inc_error_g = (double(targetImage(x,y,2)) - double(predictedImage(x,y,2)))^2;
                inc_error_b = (double(targetImage(x,y,3)) - double(predictedImage(x,y,3)))^2;
                inc_error = inc_error_r + inc_error_g + inc_error_b;
            end
            total_error = total_error + inc_error;
        end
    end
    
    total_pixels = height * width;
    mean_sq_error = double(total_error/total_pixels);
    
    psnr = 10*log10((255*255)/mean_sq_error);
end