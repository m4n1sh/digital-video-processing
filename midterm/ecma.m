function [predImage] = ecma(anchorImage,targetImage,diameter,range)
    [height width depth] = size(anchorImage);
    % mv is the motion vector matrix of dimension 2 X height/diameter X
    % width/diameter. first dimension is of size 2 to hold the x and y 
    % dimensions of motion vector of the block
    mv = zeros(2, height/diameter, width/diameter);
    predictedImage = anchorImage;
    % Start with block at (1,1) till (height-diameter, width-diameter)
    for x = 1:diameter:height - diameter
        for y = 1:diameter:width - diameter
            % We need to find the minimum MAD, so we would assign
            % min_mad to a large number
            min_mad = 1000;
            % Now for every (x,y), we need to check for matching 
            % blocks in range. Total (2R+1)
            for rx = -range:1:range
                for ry = -range:1:range
                    % we need to calculate the summation of differnece
                    % of blocks in anchor and target frame
                    mad = 0;
                    % Now we need to iterate over all the pixels in this
                    % block and calculate the differnece of intensity
                    % and add the difference to mad variable we defined
                    cx = x+(diameter/2);
                    cy = y+(diameter/2);
                    for px = x:x+diameter-1
                        % Circle is (px-cx)^2 + (py-cy)^2 = (diameter/2)^2
                        % So now for a given px, we can find the top and
                        % bottom py
                        % py = cy +- sqrt((diameter/2)^2  - (px-cx)^2)
                        ht = sqrt((diameter/2)^2 - (px-cx)^2);
                        % We now find py-top and py-bottom
                        pyt = cy + ht;
                        pyb = cy - ht;
                        for py = pyt:1:pyb
                            % We need to make sure that the corrosponding
                            % point we are searching in target frame is
                            % not out of target frame
                            if ((px+rx > 0) && (py+ry > 0) && (px+rx <= height) && (py+ry <= width))
                                difference = calc_mad(anchorImage, targetImage, px, py, px+rx, py+ry, depth);
                                if (difference > 0)
                                    mad = mad + difference;
                                end
                            end
                        end
                    end
                    % We need to divide our calculated MAD by
                    % diameter*diameter because we have 
                    % calculated summation over px * py
                    mad = mad / (diameter^2);
                    if ((x+rx > 0) && (y+ry > 0) && (x+rx <= height) && (y+ry <= width) && (mad < min_mad))
                        min_mad = mad;
                        % Save the motion vectors in X and Y coordinates
                        disp_x = rx;
                        disp_y = ry;
                    end
                end
            end
            % Find the block number for x and y
            % e.g. (1,1) would be (((1-1)/diameter)+1,
            % ((1-1)/diameter)+1)
            blk_x = ((x-1)/diameter)+1;
            blk_y = ((y-1)/diameter)+1;
            mv(1, blk_x, blk_y) = disp_x;
            mv(2, blk_x, blk_y) = disp_y;
            predictedImage(x+disp_x:x+diameter-1+disp_x, y+disp_y:y+diameter-1+disp_y) = anchorImage(x:x+diameter-1, y:y+diameter-1);
        end
    end
    %diff_img = imabsdiff(predictedImage,targetImage);
    %imshow(predictedImage);
    predImage = predictedImage;
end
