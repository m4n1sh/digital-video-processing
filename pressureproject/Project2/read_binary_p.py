# The MIT License (MIT)
#
# Copyright (c) 2013 Manish Sinha <manish.sinha@asu.edu>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import os
import argparse

def read_probablity(filename):
	with open(filename, "r") as fd:
		all_data = fd.read()
		# Not specifying any arguments to split
		# splits the string based on white space
		data_split = all_data.split()
		counter0 = 0
		counter1 = 0
		for data in data_split:
			if data == "0":
				counter0 = counter0 + 1
			if data == "1":
				counter1 = counter1 + 1
		return [counter0, counter1]

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument('filename')
	args = parser.parse_args()
	filename = args.filename
	
	if not os.path.isabs(filename):
		filename = os.path.join(os.getcwd(), filename)
	
	counter0, counter1 = read_probablity(filename)
	total = counter0 + counter1
	print("Total number of coin tosses: {0}".format(total))
	print("Total Number of times 0 (tails) and 1 (heads): {0} and {1}".format(counter0, counter1))
	print("Probability of 0 (tails) is {0}".format(float(counter0)/total))
	print("Probability of 1 (heads) is {0}".format(float(counter1)/total))	
		
