from decimal import Decimal


t=0
a=1
b=2
c=3
d=4
e=5

rev = {0:"t", 1:"a", 2:"b", 3:"c", 4:"d", 5:"e"}

dis = {}
dis[t] = {}
dis[a] = {b:-4, t:-3}
dis[b] = {d:-1, e:-2}
dis[c] = {b:8, t:3}
dis[d] = {a:6, t:4}
dis[e] = {c:-3, t:2}
#print(dis)

data = [[None for x in range(6)] for x in range(6)]
path = [ [ None for x in range(6)] for x in range(6)]
shortest = {}


for x in range(6):
	data[0][x] = Decimal('Infinity')
	for y in range(6):
		if y == 0:
			data[x][y] = Decimal('0')

#for x in range(len(data)):
#	print(x,data[x])

for i in range(6)[1:]:	# No of steps
	for j in range(6):	# Nodes
		#print(i,j)
		if data[i][j] is None:
			first = data[i-1][j]
			second = {}
			for x in dis[j]:
				#print (i,rev[j], (rev[x], int(dis[j][x]), data[i-1][x], int(dis[j][x])))
				second[data[i-1][x] + int(dis[j][x])] = x
			second_min = min(second.keys())
			#print((i, rev[j]), (first, second))
			data[i][j] = min([first, second_min])
			#if j == d:
			if data[i][j] == second_min:
				path[i][j] = second[second_min]


for x in range(6):
	for y in range(6):
		if data[x][y].is_infinite():
			print("Inf", end="\t")
		elif path[x][y] is None:
			print(int(data[x][y]), path[x][y], end="\t")
		else:
			print(int(data[x][y]), rev[path[x][y]], end="\t")
	print("\n")

for i in range(6)[1:]: # Node
	for j in range(6)[1:]:	# Iterations
		if i not in  shortest:
			shortest[i] = (data[j][i], j)
			print("Setting {0} with {1}".format(i, data[j][i]))
		else:
			#if i == 5:
			#	print(data[j][i], shortest[i][0])
			if data[j][i] < shortest[i][0]:
				shortest[i] =  (data[j][i], j)

print(shortest)

#for i in shortest:
#	print(rev[i])

def next(paths, node):
	if node == t:
		print("\n")
		return []
	short = shortest[node]
	if paths < short[1]:
		print("No path available")
		return
	print("From: {0} | Step: ({1}, {2}) | To: {3} | Distance: {4}".format(rev[node], int(short[0]),short[1], rev[path[short[1]][node]], dis[node][path[short[1]][node]]))
	next(short[1], path[short[1]][node])

next(len(data) -1, a)
