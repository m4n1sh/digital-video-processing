from django.core.management.base import BaseCommand, CommandError
from optparse import make_option
import os
import cv
from datetime import datetime

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--video',
            action='store',
            dest='video',
            default=False,
            help='The starting point'),
        ) + (
        make_option('--stats',
            action='store_true',
            dest='stats',
            default=False,
            help='Run the command without showing video and in statistical mode'),
        )
        
    def handle(self, *args, **options):
        if not options['video']:
            print("Please use --video to pass the path to the video")
            return 0 
        url = options['video']
        full_path = url if os.path.isabs(url) else os.path.join(os.getcwd(), url)
        if not os.path.exists(full_path):
            print("No video found at {0}".format(full_path))
            return 0
        
        dirname = os.path.dirname(full_path)
        filenameext = os.path.basename(full_path)
        fileName, fileExt = os.path.splitext(filenameext)
        
        self.capture = cv.CaptureFromFile(full_path)

        # Retreive the first frame
        frame = cv.QueryFrame(self.capture)
        if not frame:
            print("The video supplied is not a valid video")
            return
        
        total_frames = 1 
        start_time = datetime.now()
        
        # Create a buffer for RGB color image, 8 bit per channels, total 3 channels
        color_buffer = cv.CreateImage(cv.GetSize(frame), 8, 3)
        # Create a grey image buffer, 8 bit depth of single channel
        grey_buffer = cv.CreateImage(cv.GetSize(frame), cv.IPL_DEPTH_8U, 1)
        # Create a buffer for average of images, with 32-bit, with 3 channels
        average_buffer = cv.CreateImage(cv.GetSize(frame), cv.IPL_DEPTH_32F, 3)
        
        counter = 1
        total_contours = 0
        first = True
        while True:
            try:
                color_buffer = cv.QueryFrame(self.capture)
                if not frame:
                    break
            except:
                continue
            total_frames = total_frames + 1 

            # Run Gaussian Blue over the image buffer to remove Noise
            cv.Smooth(color_buffer, color_buffer, cv.CV_GAUSSIAN, 3, 0)

            if first:
                difference = cv.CloneImage(color_buffer)
                temp = cv.CloneImage(color_buffer)
                cv.ConvertScale(color_buffer, average_buffer, 1.0, 0.0)
                first = False
            else:
                try:
                    cv.RunningAvg(color_buffer, average_buffer, 0.020, None)
                except:
                    continue

            # Convert the scale of the moving average.
            cv.ConvertScale(average_buffer, temp, 1.0, 0.0)

            # Minus the current frame from the moving average.
            cv.AbsDiff(color_buffer, temp, difference)

            # Convert the image to grayscale.
            cv.CvtColor(difference, grey_buffer, cv.CV_RGB2GRAY)

            # Convert the image to black and white.
            cv.Threshold(grey_buffer, grey_buffer, 70, 255, cv.CV_THRESH_BINARY)

            # Dilate is important step
            cv.Dilate(grey_buffer, grey_buffer, None, 25)
            # Eroda is not necessary, but don't run Erode without first running Dilate
            cv.Erode(grey_buffer, grey_buffer, None, 20)

            storage = cv.CreateMemStorage(0)
            contour = cv.FindContours(grey_buffer, storage, cv.CV_RETR_CCOMP, cv.CV_CHAIN_APPROX_SIMPLE)
            points = []
            
            csize = 0
            while contour:
                bound_rect = cv.BoundingRect(list(contour))
                contour = contour.h_next()

                pt1 = (bound_rect[0], bound_rect[1])
                pt2 = (bound_rect[0] + bound_rect[2], bound_rect[1] + bound_rect[3])
                points.append(pt1)
                points.append(pt2)
                cv.Rectangle(color_buffer, pt1, pt2, cv.CV_RGB(255,255,0), 1)
                csize = csize + 1


            #if not options['stats']:
            cv.ShowImage("Video Contours Demo", color_buffer)
                
            counter = counter + 1
            if counter % 10 == 0:
                total_contours = total_contours + csize

            # Listen for ESC key
            c = cv.WaitKey(7) % 0x100
            if c == 27:
                break
            
        end_time = datetime.now()
        print("Number of contours per {0} frames: {1}".format(10, total_contours))
        result=os.path.join(dirname,"{0}.result".format(fileName))
        with open(result, "w+") as fd:
            fd.write(str(total_contours))
        if options['stats']:
            print("Analyzed {0} frames in {1}".format(total_frames, (end_time-start_time).total_seconds()))
