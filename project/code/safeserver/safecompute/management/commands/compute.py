from django.core.management.base import BaseCommand, CommandError
from safecore import models as core
from datetime import datetime
from optparse import make_option
from decimal import Decimal
import os

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--start',
            action='store',
            dest='start',
            default=False,
            help='The starting node'),
        ) + (
        make_option('--end',
            action='store',
            dest='end',
            default=False,
            help='End end node'),
        )
        
    def handle(self, *args, **options):
        node_count = len(core.Node.objects.all())
        if options['start'] and options['end']:
            start = None
            end = None
            try:
                start = int(options['start'])
            except ValueError:
                print("--start should be an integer")
                return
            try:
                end = int(options['end'])
            except ValueError:
                print("--end should be an integer")
                return
            if start == end:
                print("--start and --end need to be different")
                return
                
            self.__compute_all_shortest(node_count, start, end)
            return
        
        for i in range(1, node_count+1):
            for j in range(1, node_count+1):
                if i != j:
                    self.__compute_all_shortest(node_count, i, j)
                
    def __compute_all_shortest(self, node_count, x, y):
        print("Computing for {0} and {1}".format(x, y))
        map_f, map_r = self.__compute_map(node_count, x, y)
        
        weights = self.__compute_weights(map_f)

        data = self.__init_data(node_count)
        path = self.__init_path(node_count)
        
        self.__compute_data_path(node_count, data, path, weights)
        
        shortest = self.__compute_shortest(node_count, data)

        p = core.Path.objects.create(start_id=x, end_id=y, total_weight=data[node_count-1][node_count-1])
        p.save()
        
        self.next(node_count-1, node_count-1, path, shortest, map_r, weights, p)
    
    def __compute_map(self, total, s, t):
        map_f = {}
        map_r = {}

        counter = 1
        for x in range(1,total+1): 
        	if x == t:	# of x is start
		        map_f[x] = 0
		        map_r[0] = x
		        continue
        	if x == s:
		        map_f[x] = total-1
		        map_r[total-1] = x
		        continue
        	map_f[x] = counter
        	map_r[counter] = x
        	counter = counter +1
        return map_f, map_r

    def __compute_weights(self, map_f):
        weights = {}
    	for x in range(len(map_f.keys())):
            weights[x] = {}
        all_edges = core.Edge.objects.all()
        for edge in all_edges:
            edge_data = edge.get_data()
            start = map_f[int(edge_data[1].get_id())]
            end = map_f[int(edge_data[2].get_id())]
            weight = int(edge_data[3])
            weights[start][end] = weight
            weights[end][start] = weight
        return weights
	  
    def __init_data(self, node_count):
        data = [[None for x in range(node_count)] for x in range(node_count)]
        for x in range(node_count):
            data[0][x] = Decimal('Infinity')
            for y in range(node_count):
                if y == 0:
                    data[x][y] = Decimal('0')
        return data

    def __init_path(self, node_count):
        return [ [ None for x in range(node_count)] for x in range(node_count)]

    def __compute_data_path(self, node_count, data, path, dis):
        for i in range(node_count)[1:]:	# No of steps
            for j in range(node_count):	# Nodes
                if data[i][j] is None:
                    first = data[i-1][j]
                    second = {}
                    for x in dis[j]:
                        second[data[i-1][x] + int(dis[j][x])] = x
                    second_min = min(second.keys())
                    data[i][j] = min([first, second_min])
                    if data[i][j] == second_min:
                        path[i][j] = second[second_min]

    def __compute_shortest(self, node_count, data):
        shortest = {}
        for i in range(node_count)[1:]: # Node
            for j in range(node_count)[1:]:	# Iterations
                if i not in  shortest:
                    shortest[i] = (data[j][i], j)
                else:
			        if data[j][i] < shortest[i][0]:
				        shortest[i] =  (data[j][i], j)
        return shortest

    def next(self, paths, node, path, shortest, map_r, dis, pathobj):
        if node == 0:
            print("\n")
            return
        short = shortest[node]
        if paths < short[1]:
            print("No path available")
            return
        #print("From: {0} | Distance Left: {1} | To: {3} | Distance: {4}".format(map_r[node], int(short[0]),short[1], map_r[path[short[1]][node]], dis[node][path[short[1]][node]]))
        tracep = core.TraceShortestPath.objects.create(path_id=pathobj.id, end1_id=map_r[node], end2_id=map_r[path[short[1]][node]])
        tracep.save()
        self.next(short[1], path[short[1]][node], path, shortest, map_r, dis, pathobj)


