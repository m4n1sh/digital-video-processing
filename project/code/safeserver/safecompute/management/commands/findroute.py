from django.core.management.base import BaseCommand, CommandError
from safecore import models as core
from optparse import make_option

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--start',
            action='store',
            dest='start',
            default=False,
            help='The starting point'),
        ) + (
        make_option('--end',
            action='store',
            dest='end',
            default=False,
            help='The ending point'),
        )
        
    def handle(self, *args, **options):
        if not options['start'] or not options['end']:
            print("Please specify both --start and --end positions")
            return
        
        dis, route = core.Path.find_route_by_id(start=int(options['start']), end=int(options['end']))
        if dis is not None:
            print("Distance is {0}".format(dis))
            for r in route:
                print("{0} -> {1}".format(r[0], r[1]))
        else:
            print("Location {0} does not exist".format(route["node"]))
