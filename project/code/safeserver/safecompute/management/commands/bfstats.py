from django.core.management.base import BaseCommand, CommandError
from safecore import models as core
from datetime import datetime
import os
from safecompute.management.commands import compute
import json
import numpy as np
from optparse import make_option
import matplotlib.pyplot as plt

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--data',
            action='store',
            dest='data',
            default=False,
            help='The stored stats'),
        )
        
    def handle(self, *args, **options):

        if options['data']:
            if not os.path.exists(options['data']):
                print("The provided file cannot be found: {0}".format(options['data']))
                return
            with open(options['data'], "r") as fd:
                data = fd.read()
                timetaken = json.loads(data)
                self.__create_chart(timetaken)
                return
                
        node_count = len(core.Node.objects.all())
        timetaken = {}
        counter = 0
        cobj = compute.Command()
        for i in range(1, node_count+1):
            for j in range(1, node_count+1):
                if i != j:
                    start_time = datetime.now()
                    cobj.handle(node_count, start=i, end=j)
                    end_time = datetime.now()
                    diff = end_time-start_time
                    timetaken[counter] = diff.total_seconds()*1000
                    counter = counter + 1
        with open("stats.json", "w+") as fd:
            fd.write(json.dumps(timetaken))
        self.__create_chart(timetaken)
        
        
    def __create_chart(self, timetaken):
        font = {
            'family' : 'serif',
            'color'  : 'darkred',
            'weight' : 'normal',
            'size'   : 16,
        }
        x = list(timetaken.keys())
        y = list(timetaken.values())
        plt.plot(x, y, 'k')
        plt.title('Time taken for Bellman-Ford for every two nodes', fontdict=font)
        plt.xlabel('Path ID', fontdict=font)
        plt.ylabel('time (milliseconds)', fontdict=font)

        # Tweak spacing to prevent clipping of ylabel
        plt.subplots_adjust(left=0.15)
        plt.savefig('bfstats.png')
        print("The plot has been saved at {0}".format(os.path.join(os.getcwd(), "bfstats.png")))
        
        val = timetaken.values()
        minimum = min(val)
        min_key = None
        maximum = max(val)
        max_key = None
        mean = np.mean(val)
        
        print("Max: {0} | Min: {1} | Mean: {2}".format(maximum, minimum, mean))
                    
                    
        
           
