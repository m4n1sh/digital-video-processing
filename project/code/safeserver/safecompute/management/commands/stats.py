from django.core.management.base import BaseCommand, CommandError
from safecore import models as core
from datetime import datetime
import numpy as np
import matplotlib.pyplot as plt
import os

class Command(BaseCommand):
    def handle(self, *args, **options):
        nodes = 42
        stats = {}
        routes = {}
        font = {'family' : 'serif',
        'color'  : 'darkred',
        'weight' : 'normal',
        'size'   : 16,
        }

        for start in range(1, nodes+1):
            for end in range(1, nodes+1):
                if start != end:
                    start_time = datetime.now()
                    dis, route = core.Path.find_route_by_id(start=start, end=end)
                    end_time = datetime.now()
                    key = "{0}->{1}".format(start,end)
                    stats[key] = (end_time-start_time).microseconds
                    routes[key] = (dis, route)
        
        val = stats.values()
        minimum = min(val)
        min_key = None
        maximum = max(val)
        max_key = None
        mean = np.mean(val)
        
        for key, value in stats.iteritems():
            if value == minimum:
                min_key = key
            if value == maximum:
                max_key = key
        print("Max: {0} - {1} - {2} - {3}".format(max_key, maximum, routes[max_key][0], len(routes[max_key][1])))
        print("Min: {0} - {1} - {2} - {3}".format(min_key, minimum, routes[min_key][0], len(routes[min_key][1])))
        print("Mean: {0}".format(mean))
        
        x = range(1, len(stats.keys())+1)
        y = stats.values()
        plt.plot(x, y, 'k')
        plt.title('Time taken for every path query', fontdict=font)
        plt.xlabel('Path ID', fontdict=font)
        plt.ylabel('time (microseconds)', fontdict=font)

        # Tweak spacing to prevent clipping of ylabel
        plt.subplots_adjust(left=0.15)
        plt.savefig('work.png')
        print("The plot has been saved at {0}".format(os.path.join(os.path.getcwd(), "work.png")))
           
