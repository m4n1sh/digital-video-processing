from decimal import Decimal


map_r = {0: 38, 1: 1, 2: 2, 3: 3, 4: 4, 5: 5, 6: 6, 7: 7, 8: 8, 9: 9, 10: 10, 11: 11, 12: 12, 13: 13, 14: 14, 15: 15, 16: 16, 17: 17, 18: 18, 19: 19, 20: 20, 21: 21, 22: 22, 23: 23, 24: 24, 25: 25, 26: 26, 27: 27, 28: 28, 29: 29, 30: 30, 31: 31, 32: 33, 33: 34, 34: 35, 35: 36, 36: 37, 37: 39, 38: 40, 39: 41, 40: 42, 41: 32}
dis = {0: {9: 1, 10: 10, 36: 10, 37: 1}, 1: {5: 1, 19: 1}, 2: {9: 1, 10: 1}, 3: {11: 1, 16: 1}, 4: {12: 1, 17: 1}, 5: {1: 1, 6: 1, 20: 10}, 6: {5: 1, 7: 1, 23: 20}, 7: {6: 1, 8: 1, 40: 10}, 8: {7: 1, 9: 1, 36: 30}, 9: {0: 1, 2: 1, 8: 1}, 10: {0: 10, 2: 1, 38: 1}, 11: {3: 1, 38: 1, 39: 1}, 12: {4: 1, 13: 1, 25: 40}, 13: {12: 1, 14: 1, 26: 40}, 14: {13: 1, 15: 1, 27: 30}, 15: {14: 1, 16: 1, 28: 1}, 16: {3: 1, 15: 1, 30: 20}, 17: {4: 1, 18: 1, 24: 40}, 18: {17: 1, 19: 1, 21: 10}, 19: {1: 1, 18: 1, 20: 10}, 20: {5: 10, 19: 10, 21: 10}, 21: {18: 10, 20: 10, 22: 40}, 22: {21: 40, 23: 30, 41: 70}, 23: {6: 20, 22: 30, 40: 40}, 24: {17: 40, 25: 40, 41: 70}, 25: {12: 40, 24: 40, 26: 50}, 26: {13: 40, 25: 50, 27: 40, 32: 30}, 27: {14: 30, 26: 40, 28: 50, 35: 20}, 28: {15: 1, 27: 50, 29: 1, 30: 60}, 29: {28: 1, 37: 1, 39: 1}, 30: {16: 20, 28: 60, 31: 50}, 31: {30: 50, 39: 50}, 32: {26: 30, 34: 60, 40: 40, 41: 30}, 33: {34: 80, 36: 70, 40: 70}, 34: {32: 60, 33: 80, 35: 60}, 35: {27: 20, 34: 60, 36: 50, 37: 40}, 36: {0: 10, 8: 30, 33: 70, 35: 50}, 37: {0: 1, 29: 1, 35: 40, 38: 40}, 38: {10: 1, 11: 1, 37: 40}, 39: {11: 1, 29: 1, 31: 50}, 40: {7: 10, 23: 40, 32: 40, 33: 70}, 41: {22: 70, 24: 70, 32: 30}}
node_count = len(dis.keys())
#print(dis)

data = [[None for x in range(node_count)] for x in range(node_count)]
path = [ [ None for x in range(node_count)] for x in range(node_count)]
shortest = {}


for x in range(node_count):
	data[0][x] = Decimal('Infinity')
	for y in range(node_count):
		if y == 0:
			data[x][y] = Decimal('0')

#for x in range(len(data)):
#	print(x,data[x])

for i in range(node_count)[1:]:	# No of steps
	for j in range(node_count):	# Nodes
		#print(i,j)
		if data[i][j] is None:
			first = data[i-1][j]
			second = {}
			for x in dis[j]:
				#print (i,rev[j], (rev[x], int(dis[j][x]), data[i-1][x], int(dis[j][x])))
				second[data[i-1][x] + int(dis[j][x])] = x
			second_min = min(second.keys())
			#print((i, rev[j]), (first, second))
			data[i][j] = min([first, second_min])
			#if j == d:
			if data[i][j] == second_min:
				path[i][j] = second[second_min]


print(data[41][41])

for i in range(node_count)[1:]: # Node
	for j in range(node_count)[1:]:	# Iterations
		if i not in  shortest:
			shortest[i] = (data[j][i], j)
		else:
			#if i == 5:
			#	print(data[j][i], shortest[i][0])
			if data[j][i] < shortest[i][0]:
				shortest[i] =  (data[j][i], j)

print(shortest)

def next(paths, node):
	if node == 0:
		print("\n")
		return
	short = shortest[node]
	if paths < short[1]:
		print("No path available")
		return
	print("From: {0} | Step: ({1}, {2}) | To: {3} | Distance: {4}".format(map_r[node], int(short[0]),short[1], map_r[path[short[1]][node]], dis[node][path[short[1]][node]]))
	next(short[1], path[short[1]][node])

next(node_count-1, 41)

