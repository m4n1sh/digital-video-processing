from django.conf.urls import patterns, include, url

from safecore.views import RouteView, SearchView, IndexView, ShowRouteView
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'safeserver.views.home', name='home'),
    # url(r'^safeserver/', include('safeserver.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
     url(r'^admin/', include(admin.site.urls)),
     url(r'^route/$', RouteView.as_view(), name='route-view'),
     url(r'^search/$', SearchView.as_view(), name='search-view'),
     url(r'^showroute/$', ShowRouteView.as_view(), name='show-route-view'),
     url(r'^$', IndexView.as_view(), name='index-view'),
)
