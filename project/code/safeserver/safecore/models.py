from django.db import models

# The primary models
class Node(models.Model):
    x = models.IntegerField()
    x_min = models.FloatField()
    y = models.IntegerField()
    y_min = models.FloatField()
    comment = models.TextField()
    
    def get_id(self):
    	return self.id
    
    def __unicode__(self):
    	return self.comment

class Edge(models.Model):
    start = models.ForeignKey(Node, related_name='start')
    end = models.ForeignKey(Node, related_name='end')
    weight = models.IntegerField()

    def __unicode__(self):
        return "{0} | {1}".format(self.start, self.end)
    
    def get_data(self):
    	return self.id, self.start, self.end, self.weight

class Path(models.Model):
    start = models.ForeignKey(Node, related_name='first_edge')
    end = models.ForeignKey(Node, related_name='second_edge')
    total_weight = models.IntegerField()
    
    @staticmethod
    def find_route_by_id(start, end):
        start_node = Node.objects.filter(id=start)
        end_node = Node.objects.filter(id=end)
        if not start_node:
            return None, {"node": start, "valid": False}
        if not end_node:
            return None, {"node": end, "valid": False}
        print(start_node, end_node)
        ps = Path.objects.filter(start_id=start, end_id=end)
        if not ps:
            return None, {"node": None, "valid": False}
        p = ps[0]
        paths = TraceShortestPath.objects.filter(path_id=p.id)
        return p.total_weight, [(path.end1, path.end2) for path in paths]
    
    @staticmethod
    def find_route(start, end):
        dis, route = Path.find_route_by_id(start=start, end=end)
    	if dis is None:
    		return route
        path_dict = {}
    	counter = 1;
    	for end1, end2 in route:
    		start_dict = {"id": end1.id, "comment": end1.comment, "x": end1.x, "xm": end1.x_min, "y": end1.y, "ym": end1.y_min}
    		end_dict = {"id": end2.id, "comment": end2.comment, "x": end2.x, "xm": end2.x_min, "y": end2.y, "ym": end2.y_min}
    		path_dict[counter] = {"start": start_dict, "end": end_dict}
    		counter = counter + 1
    	for route_id in path_dict:
    	    entry = path_dict[route_id]
    	    entry["start"]["xx"] = float("{0}.{1}".format(entry["start"]["x"], str(entry["start"]["xm"]/60)[2:]))
    	    entry["start"]["yy"] = float("{0}.{1}".format(entry["start"]["y"], str(entry["start"]["ym"]/60)[2:]))
    	    entry["end"]["xx"] = float("{0}.{1}".format(entry["end"]["x"], str(entry["end"]["xm"]/60)[2:]))
    	    entry["end"]["yy"] = float("{0}.{1}".format(entry["end"]["y"], str(entry["end"]["ym"]/60)[2:]))
    	response = {"valid": True, "distance": dis, "path": path_dict}
    	return response

class TraceShortestPath(models.Model):
	path = models.ForeignKey(Path)
	end1 = models.ForeignKey(Node, related_name='first_end')
	end2 = models.ForeignKey(Node, related_name='second_end')

# Secondary model

class CumulativeWeight(models.Model):
    edge = models.ForeignKey(Edge)
    cweight = models.BigIntegerField() # The cumulative weight
    ctime = models.BigIntegerField() # The cumulative time
