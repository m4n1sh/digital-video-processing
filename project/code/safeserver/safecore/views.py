# Create your views here.

from django.http import HttpResponse, HttpResponseNotFound
from django.views.generic import View
from django.shortcuts import render
from safecore.models import Node, Path
import json

class RouteView(View):
    def get(self, request, *args, **kwargs):
        response = {"valid": True, "keys": []}
        if 'start' not in request.GET:
            response["valid"] = False
            response["keys"].append("start")
        if 'start' not in request.GET:
            response["valid"] = False
            response["keys"].append("end")
        if not response["valid"]:
            return HttpResponseNotFound(json.dumps(response))
            
    	dis, route = Path.find_route_by_id(start=int(request.GET['start']), end=int(request.GET['end']))
    	if dis is None:
    		return HttpResponseNotFound(json.dumps(route))
    		
    	path_dict = {}
    	counter = 1;
    	for end1, end2 in route:
    		start_dict = {"id": end1.id, "comment": end1.comment, "x": end1.x, "xm": end1.x_min, "y": end1.y, "ym": end1.y_min}
    		end_dict = {"id": end2.id, "comment": end2.comment, "x": end2.x, "xm": end2.x_min, "y": end2.y, "ym": end2.y_min}
    		path_dict[counter] = {"start": start_dict, "end": end_dict}
    		counter = counter + 1
    	response = {"valid": True, "distance": dis, "path": path_dict}
        return HttpResponse(json.dumps(response))

    def post(self, request, *args, **kwargs):
        return HttpResponse('This is POST request')

class SearchView(View):
    def get(self, request, *args, **kwargs):
        if 'term' not in request.GET:
            return HttpResponseNotFound(json.dumps({"valid": False, "keys": "term"}))
        
        node_result = Node.objects.filter(comment__icontains=request.GET['term'])
        nodes = {}
        for node in node_result:
            nodes[node.id] = node.comment
        return HttpResponse(json.dumps(nodes))

class ShowRouteView(View):
    def post(self, request, *args, **kwargs):
        route = Path.find_route(start=int(request.POST['from']), end=int(request.POST['to']))
        if not route["valid"]:
            if not route["node"]:
                return render(request, 'safecore/showerror.htm')
    		return HttpResponseNotFound(json.dumps(route))
    	
    	flattened_route = []
    	flattened_route_comment = {}
    	flattened_route_keys = []
    	for key in route["path"]:
    	    value = route["path"][key]
    	    start = value["start"]
    	    end = value["end"]
    	    if int(start["id"]) not in flattened_route_keys:
    	        flattened_route.append( [ int(start["id"]), start["xx"], start["yy"] ] )
    	        flattened_route_comment[int(start["id"])] = start["comment"]
    	        flattened_route_keys.append(int(start["id"]))
    	    if int(end["id"]) not in flattened_route_keys:
    	        flattened_route.append( [ int(end["id"]), end["xx"], end["yy"] ] )
    	        flattened_route_comment[int(end["id"])] = end["comment"]
    	        flattened_route_keys.append(int(end["id"]))
    	print(flattened_route)
        context = {"path": route["path"], "path_dump": json.dumps(route["path"]), "flattened_route": flattened_route, "route_name": json.dumps(flattened_route_comment)}
        return render(request, 'safecore/showmap.htm', context)
        #return HttpResponse(json.dumps(route["path"]))
        
class IndexView(View):
    def get(self, request, *args, **kwargs):
        node_result = Node.objects.all()
        context = {"node_result": node_result}
        return render(request, 'safecore/map.htm', context)
