from django.contrib import admin
from safecore.models import Node, Edge, Path, CumulativeWeight

admin.site.register(Node)
admin.site.register(Edge)
admin.site.register(Path)
admin.site.register(CumulativeWeight)
