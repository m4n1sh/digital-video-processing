import json

s = 32
t = 38

map_f = {}
map_r = {}
total = 42

counter = 1
for x in range(1,total+1):
	if x == t:	# of x is start
		map_f[x] = 0
		map_r[0] = x
		continue
	if x == s:
		map_f[x] = total-1
		map_r[total-1] = x
		continue
	map_f[x] = counter
	map_r[counter] = x
	counter = counter +1

#print(map_f)
#print(map_r)

with open("map_f", "w") as fd:
	fd.write(json.dumps(map_f))

with open("map_r", "w") as fd:
	fd.write(json.dumps(map_r))

with open("weights", "r") as fd, open("WeightDict.json", "w") as fd_out:
	weights = {}
	for x in range(42):
		weights[x] = {}
	for line in fd:
		contents = line.split(",")
		start = map_f[int(contents[1])]
		end = map_f[int(contents[2])]
		weight = int(contents[3].strip())
		weights[start][end] = weight
		weights[end][start] = weight
	fd_out.write(json.dumps(weights, sort_keys=True))
	
	for x in weights:
		print(map_r[x], end="\t")
		for y in weights[x]:
			print("{0} -> {1}".format(map_r[y], weights[x][y]), end=" | ")
		print("\n")
