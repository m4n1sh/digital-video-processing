import json

with open("nodes", "r") as fd, open("Node.json", "w") as fd_out:
    maps = []
    for line in fd:
        contents = line.split(",")
        data = {}
        data["model"] = "safecore.node"
        data["pk"] = int(contents[0])
        fields = {}
        fields["x"] = int(contents[1])
        fields["x_min"] = float(contents[2])
        fields["y"] = int(contents[3])
        fields["y_min"] = float(contents[4])
        fields["comment"] = contents[5].strip()
        data["fields"] = fields
        maps.append(data)
    fd_out.write(json.dumps(maps, sort_keys=True, indent=4))

with open("edges", "r") as fd, open("Edge.json", "w") as fd_out:
    maps = []
    for line in fd:
        contents = line.split(",")
        data = {}
        data["model"] = "safecore.edge"
        data["pk"] = int(contents[0])
        fields = {}
        fields["start"] = int(contents[1])
        fields["end"] = int(contents[2].strip())
        fields["weight"] = 1
        data["fields"] = fields
        maps.append(data)
    fd_out.write(json.dumps(maps, sort_keys=True, indent=4))

with open("weights", "r") as fd, open("EdgeWeight.json", "w") as fd_out:
    maps = []
    for line in fd:
        contents = line.split(",")
        data = {}
        data["model"] = "safecore.edge"
        data["pk"] = int(contents[0])
        fields = {}
        fields["start"] = int(contents[1])
        fields["end"] = int(contents[2])
        fields["weight"] = int(contents[3].strip())
        data["fields"] = fields
        maps.append(data)
    fd_out.write(json.dumps(maps, sort_keys=True, indent=4))
