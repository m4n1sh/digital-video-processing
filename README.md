Digital Video Processing
==========================

This repository contains all my work related to CSE 509 Digital Video Processing 
taken at Arizona State University in Fall of 2013 under Prof Hari Sundaram.

This repository contains:

* Pressure Projects
* Mini Project
* Midterm Exam
* Paper Presentation
* Project Proposal
* Project
* Project Presentation

Pressure Projects
------------------

Pressure Projects are a format of a project where the question is open ended 
and the answers have to be deduced from creativity and open critical thinking. 
The time allocated for the project is usually 24 hours and consists of 5 marks. 
A total of 6 pressure projects each having 5% weightage are supposed to be 
assigned as per the syllabus which together adds us to 30% of the overall weightage.

There is a bonus Pressure Project (seventh) which weights 5%.

Pressure Project Questions:

* [One](https://bitbucket.org/m4n1sh/dvp/src/fcf761fa9e1b95d3067cba4b82010cc8a0bcab17/pressureproject/Project1/pressure%20project%2001.pdf?at=master)
* [Two](https://bitbucket.org/m4n1sh/dvp/src/fcf761fa9e1b95d3067cba4b82010cc8a0bcab17/pressureproject/Project2/pressure%20project%2002.pdf?at=master)
* [Three](https://bitbucket.org/m4n1sh/dvp/src/fcf761fa9e1b95d3067cba4b82010cc8a0bcab17/pressureproject/Project3/pressure%20project%2003.pdf?at=master)
* [Four](https://bitbucket.org/m4n1sh/dvp/src/fcf761fa9e1b95d3067cba4b82010cc8a0bcab17/pressureproject/Project4/pressure%20project%2004.pdf?at=master)
* [Five](https://bitbucket.org/m4n1sh/dvp/src/fcf761fa9e1b95d3067cba4b82010cc8a0bcab17/pressureproject/Project5/pressure%20project%2005.pdf?at=master)
* [Six](https://bitbucket.org/m4n1sh/dvp/src/fcf761fa9e1b95d3067cba4b82010cc8a0bcab17/pressureproject/Project6/pressure%20project%2006.pdf?at=master)
* [Seven](https://bitbucket.org/m4n1sh/dvp/src/fcf761fa9e1b95d3067cba4b82010cc8a0bcab17/pressureproject/Project7/pressure%20project%2007.pdf?at=master)


[Mini Project](https://bitbucket.org/m4n1sh/dvp/src/fcf761fa9e1b95d3067cba4b82010cc8a0bcab17/miniproject/mini%20project-1.pdf?at=master)
-------------

The mini project contains implementation of Exhaustive Block Matching 
Estimation (EBMA), 2D-Log Search Estimation and Multi-Resolution Technique for 
motion estimation.

The code is written in MATLAB and the algorithm is executed over all the four 
image sequences provided at the webpage of [HEDVIG KJELLSTRÖM](http://www.nada.kth.se/~hedvig/data.html)

The algorithm reports Execution Time and PSNR on every pair of images. The 
algoriithm needs to make necessary assumptions and such assumptions needs to be 
properly justified.

[Midterm Exam](https://bitbucket.org/m4n1sh/dvp/src/fcf761fa9e1b95d3067cba4b82010cc8a0bcab17/midterm/mid-term%2C%20fall%202013%2C%20cse509.pdf?at=master)
-------------


The midterm contains a bunch of questions and new algorithm which is a 
variation of EBMA. In place of square used in EBMA, we now use a circle, which 
leads to our algorithm being called Exhaustive Circle-based Motion 
Estimation (ECMA). Just like the Mini Project, the PSNR and Running time needs 
to be computed and compared with EBMA.

Paper Presentation
--------------------

As per the requirement of the course, we all needed to present two papers 
which have been published. We all were given 9-10 minutes to present another 
author's paper and the presentation needs to contain the follow outline

* Introduction
* Related Work
* The contents of the paper
* Experiment and Results
* Advantages
* Disadvantages
* Summary

The two papers presented by me were

1. Gupta, Mohit, Qi Yin, and Shree K. Nayar. [*"Structured Light In Sunlight."*](https://bitbucket.org/m4n1sh/dvp/src/913a9039583ceced7417ba22923915f11a46fc23/presentations/1_structured_light_sunlight/1_structured_light_sunlight.pdf?at=master)
2. Fernando, Basura, and Tinne Tuytelaars. [*"Mining Multiple Queries for Image Retrieval: On-the-fly learning of an Object-specific Mid-level Representation."*](https://bitbucket.org/m4n1sh/dvp/src/913a9039583ceced7417ba22923915f11a46fc23/presentations/2_multiple_query_image_retreival/2_multiple_query_image_retreival.pdf?at=master)

Project
--------

The abstract of the project is as follows. It is called "SafeCampus"

> In this project we would like to solve the problem
> of finding the safest walking route between two places on a map.
> We compute the safety of a path based on the total number of
> people walking or biking on that route. The more the number of
> people, it is considered the safest. The requirement arose out
> of finding safest path in the University Campus at dusk or
> night. A lot of Universities have close circuit camera installed
> at important locations. Using the video camera feed, we would
> have enough data to compute the relative safety of various lanes
> of the University Campus.
> The system would be kept updating on fixed intervals and would
> be implemented as a web service with a public API, such that
> clients can be written for various platforms.

The folder of Project contains the following:

1. [**proposal**](https://bitbucket.org/m4n1sh/dvp/src/ee61b7f923c3a408f7195e79fdba9d9bd506eb48/project/proposal/dvp_project_proposal.pdf?at=master) - Proposal for the Project
2. [**code**](https://bitbucket.org/m4n1sh/dvp/src/ee61b7f923c3a408f7195e79fdba9d9bd506eb48/project/code/safeserver/?at=master) - Code of the project written using Django
3. [**presentation**](https://bitbucket.org/m4n1sh/dvp/src/ee61b7f923c3a408f7195e79fdba9d9bd506eb48/project/presentation/DVP-Final-Presentation.pdf?at=master) - Presentation in both odp and pdf format
4. [**report**](https://bitbucket.org/m4n1sh/dvp/src/ee61b7f923c3a408f7195e79fdba9d9bd506eb48/project/report/FinalReport.pdf?at=master) - A five page report of SafeCampus
5. **portfolio** - A condensed three page report of SafeCampus

