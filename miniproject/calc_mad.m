function difference = calc_mad(anchorImage, targetImage, px, py, tx, ty, depth)
    difference =0;
    if depth == 3
        ai = anchorImage(px,py,:);
        ti = targetImage(tx,ty,:);
        difference = sqrt(double((int16(ti(1))-int16(ai(1)))^2 + (int16(ti(2))-int16(ai(2)))^2 + (int16(ti(3))-int16(ai(3)))^2));
    end
    if depth == 1
        difference = targetImage(tx, ty) - anchorImage(px,py);
    end
end
