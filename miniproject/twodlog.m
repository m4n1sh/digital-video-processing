function [motionVect, predImage] = twodlog(anchorImage,targetImage,blocksize,range)
    [height width depth] = size(anchorImage);
    % mv is the motion vector matrix of dimension 2 X height/blocksize X
    % width/blocksize. first dimension is of size 2 to hold the x and y 
    % dimensions of motion vector of the block
    mv = zeros(2, height/blocksize, width/blocksize);
    predictedImage = anchorImage;
    diamond(1,:) = [-1,0];
    diamond(2,:) = [1,0];
    diamond(3,:) = [0,0];
    diamond(4,:) = [0,1];
    diamond(5,:) = [0,-1];
    diamond(6,:) = [-1,-1];
    diamond(7,:) = [-1,1];
    diamond(8,:) = [1,-1];
    diamond(9,:) = [1,1];
    
    % Start with block at (1,1) till (height-blocksize, width-blocksize)
    for x = 1:blocksize:height - blocksize
        for y = 1:blocksize:width - blocksize
            % We need to find the minimum MAD, so we would assign
            % min_mad to a large number
            min_mad = 1000;
            
            i = x;
            j = y;
            searchsize = range/2;
            while searchsize > 0
                if searchsize == 1
                    searchlimit = 9;
                else
                    searchlimit = 5;
                end
                for z = 1:searchlimit
                    rx = diamond(z,1)*searchsize;
                    ry = diamond(z,2)*searchsize;
                    
                    % we need to calculate the summation of differnce
                    % of blocks in anchor and target frame
                    mad = 0;
                    % Now we need to iterate over all the pixels in this
                    % block and calculate the differnece of intensity
                    % and add the difference to mad variable we defined
                    for px = 1:blocksize
                        for py = 1:blocksize
                            ax = x+px-1;
                            ay = y+py-1;
                            tx = i+px+rx-1;
                            ty = j+py+ry-1;
                            % We need to make sure that the corrosponding
                            % point we are searching in target frame is
                            % not out of target frame
                            if ((tx > 0) && (ty > 0) && (tx <= height) && (ty <= width))
                                difference = calc_mad(anchorImage, targetImage, ax, ay, tx, ty, depth);
                                if (difference > 0)
                                    mad = mad + difference;
                                end
                            end
                        end
                    end
                    % We need to divide our calculated MAD by
                    % blocksize*blocksize because we have 
                    % calculated summation over px * py
                    mad = mad / (blocksize^2);
                    if ((i+rx > 0) && (j+ry > 0) && (i+rx <= height) && (j+ry <= width) && (mad < min_mad))
                        min_mad = mad;
                        % Save the motion vectors in X and Y coordinates
                        disp_x = rx;
                        disp_y = ry;
                    end
                    
                end
                
                % If searchsize is 1, then it means that we have checked
                % all 9 points.
                if searchsize == 1
                    break;
                end
                % If it is still on the same spot, we need to reduce by half
                if ((disp_x == 0) && (disp_y ==0))
                    searchsize = searchsize/2;
                else
                    new_i = i + disp_x;
                    new_j = j + disp_y;
                    if ((abs(new_i - x) == range) || (abs(new_j - y) == range))
                        searchsize = searchsize/2;
                        if (abs(new_i - x) == range)
                            i = i + disp_x/2;
                        end
                        if (abs(new_j - x) == range)
                            j = j + disp_y/2;
                        end
                    else
                        i = new_i;
                        j = new_j;
                    end
                end
            end
                
            % Find the block number for x and y
            % e.g. (1,1) would be (((1-1)/blocksize)+1,
            % ((1-1)/blocksize)+1)
            blk_x = ((x-1)/blocksize)+1;
            blk_y = ((y-1)/blocksize)+1;
            mv(1, blk_x, blk_y) = disp_x;
            mv(2, blk_x, blk_y) = disp_y;
            predictedImage(x+disp_x:x+blocksize-1+disp_x, y+disp_y:y+blocksize-1+disp_y) = anchorImage(x:x+blocksize-1, y:y+blocksize-1);
        end
    end
    %diff_img = imabsdiff(predictedImage,targetImage);
    %imshow(predictedImage);
    motionVect = mv;
    predImage = predictedImage;
end
