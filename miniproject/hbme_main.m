function [hbme_res, motion_vector] = hbme_main(input_path, output_path)
    % Get all the tif files in the directory
    filematch = dir(fullfile(input_path, '*.tif'));
    % Get all the file names and split them
    allfiles = {filematch.name}';
    % Find the total number of files. Each file corrosponds to one frame
    totalframes = numel(allfiles);
    hbme_data = zeros(totalframes-1,2);
    blocksize = 20;

    %anchor = imread(fullfile(filepath, 'frame0044.tif'));
    %target = imread(fullfile(filepath, 'frame0045.tif'));
    %[mv, predictedImage] = ebma(anchor, target, 16, 3);
    % Iterate over all the images
    for i = 1:totalframes-1
        tic
        anchor = imread(fullfile(input_path, allfiles{i}));
        target = imread(fullfile(input_path, allfiles{i+1}));
        
        % Create Level 1
        l1_anchor = imresize(anchor, 0.25);
        l1_target = imresize(target, 0.25);
        l1_mv = ebma(l1_anchor, l1_target, blocksize, 1);
        
        % Create Level 2
        l2_anchor = imresize(anchor, 0.5);
        l2_target = imresize(target, 0.5);
        l2_predicted = imresize(anchor, 0.5);
        [l2_height, l2_width, depth] = size(l2_anchor);
        l2_mv = zeros(2, l2_height/blocksize, l2_width/blocksize);
        for x = 1:blocksize:l2_height-blocksize
            for y = 1:blocksize:l2_width-blocksize
                bx = floor((x-1)/(blocksize*2)) + 1;
                by = floor((y-1)/(blocksize*2)) + 1;
                mvbx = l1_mv(1, bx, by) * 2;
                mvby = l1_mv(2, bx, by) * 2;
                l2_mv(1, bx, by) = mvbx;
                l2_mv(2, bx,by) = mvby;
                l2_predicted(x+mvbx:x+mvbx+blocksize-1, y+mvby:y+mvby+blocksize-1) = l2_anchor(x:x+blocksize-1, y:y+blocksize-1);
            end
        end
        l2_mv_adj = ebma(l2_predicted, l2_target, blocksize, 1);
        l2_mv(1,:,:) = l2_mv(1,:,:) + l2_mv_adj(1,:,:);
        l2_mv(2,:,:) = l2_mv(2,:,:) + l2_mv_adj(2,:,:);
        
        % Create Level 3
        l3_predicted = anchor;
        [l3_height, l3_width, depth] = size(anchor);
        l3_mv = zeros(2, l3_height/blocksize, l3_width/blocksize);
        for x = 1:blocksize:l3_height-blocksize
            for y = 1:blocksize:l3_width-blocksize
                bx = floor((x-1)/(blocksize*2)) + 1;
                by = floor((y-1)/(blocksize*2)) + 1;
                mvbx = l2_mv(1, bx, by) * 2;
                mvby = l2_mv(2, bx, by) * 2;
                l3_mv(1, bx, by) = mvbx;
                l3_mv(2, bx,by) = mvby;
                l3_predicted(x+mvbx:x+mvbx+blocksize-1, y+mvby:y+mvby+blocksize-1) = anchor(x:x+blocksize-1, y:y+blocksize-1);
            end
        end
        l3_mv_adj = ebma(l3_predicted, target, blocksize, 1);
        l3_mv(1,:,:) = l3_mv(1,:,:) + l3_mv_adj(1,:,:);
        l3_mv(2,:,:) = l3_mv(2,:,:) + l3_mv_adj(2,:,:);
        
        % Create the final image
        predictedImage = anchor;
        for x = 1:blocksize:l3_height-blocksize
            for y = 1:blocksize:l3_width-blocksize
                bx = floor((x-1)/(blocksize*2)) + 1;
                by = floor((y-1)/(blocksize*2)) + 1;
                mvbx = l3_mv(1, bx, by);
                mvby = l3_mv(2, bx, by);
                predictedImage(x+mvbx:x+mvbx+blocksize-1, y+mvby:y+mvby+blocksize-1) = anchor(x:x+blocksize-1, y:y+blocksize-1);
            end
        end
        
        hbme_data(i,2) = toc;
        
        psnr = computer_psnr(target, predictedImage);
        hbme_data(i,1) = psnr;
        
        imwrite(predictedImage, fullfile(output_path, allfiles{i}));
    end
    hbme_res = hbme_data;
    motion_vector = l3_mv;
end
