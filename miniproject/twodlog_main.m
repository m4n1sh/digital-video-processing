function [twodlog_res, motion_vector] = twodlog_main(input_path, output_path)
    % Get all the tif files in the directory
    filematch = dir(fullfile(input_path, '*.tif'));
    % Get all the file names and split them
    allfiles = {filematch.name}';
    % Find the total number of files. Each file corrosponds to one frame
    totalframes = numel(allfiles);
    twodlog_data = zeros(totalframes-1,2);

    %anchor = imread(fullfile(filepath, 'frame0044.tif'));
    %target = imread(fullfile(filepath, 'frame0045.tif'));
    %[mv, predictedImage] = ebma(anchor, target, 16, 3);
    % Iterate over all the images
    for i = 1:totalframes-1
        tic
        anchor = imread(fullfile(input_path, allfiles{i}));
        target = imread(fullfile(input_path, allfiles{i+1}));
        [mv, predictedImage] = twodlog(anchor, target, 16, 4);
        twodlog_data(i,2) = toc;
        
        psnr = computer_psnr(target, predictedImage);
        twodlog_data(i,1) = psnr;
        
        imwrite(predictedImage, fullfile(output_path, allfiles{i}));
    end
    twodlog_res = twodlog_data;
    motion_vector = mv;
end
